<a href="https://translate.fedoraproject.org/engage/fedora-websites-3-0/">
<img src="https://translate.fedoraproject.org/widgets/fedora-websites-3-0/-/svg-badge.svg" alt="Translation status" />
</a>

## Bugs:

- Design questions and discussion: https://gitlab.com/fedora/design/team/wwwfpo-2022/-/issues
- Bugs (ie. logo squished on mobile) https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0/-/issues
- Who's working on what: https://gitlab.com/groups/fedora/websites-apps/fedora-websites/-/boards/4623394

Note: To edit the content of this site you do not need below guide - simply go to the CMS linked in header. To develop new features or add new pages, read on.

## Development: Getting Started

This repository was set up on July 20 2022 as the team voted on Fedora's static frontend pages.

It is an off-the-shelf `create-nuxt-app` project. For more information see [nuxt documentation](https://nuxt.com/docs).

- Clone this repository and run in the folder:

  - `npm install`: to install the project dependencies
  - [`npm run dev`](https://v3.nuxtjs.org/api/commands/dev): to run a live development server on localhost:3000
  - [`npm run generate`](https://v3.nuxtjs.org/api/commands/generate): creates pre-rendered pages for static hosting in .output/public (this is what the CI does too)


### Vue Training Videos for Fedora Websites developers

We have a two-part training video series that takes you from start to finish implementing front end components using Vue.js starting with one of our design mockups and finishing with code. Part 1 is complete and part 2 is in editing:

- [How to create vue components for the new Fedora Website - Part 1](https://peertube.linuxrocks.online/w/9c6NkDP8vLnH2eWgrWasaw)

---

## General Vue Setup

- vscode extensions:
  - [volar](https://github.com/johnsoncodehk/volar) (recommended to install from extensions menu)
  - [Steps to set up take over mode for better typescript support](https://github.com/johnsoncodehk/volar/discussions/471)
- [codium rpm alternative to vscode](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo)
  - For those who want to not use a microsoft code editor but still want the plugins **and** a functioning integrated terminal
- Or use vscode. Flatpak versions are generally fine as well, however the limited integrated terminal can cause issues
- [Check out the Vue Documentation for Vue syntax](https://vuejs.org/)

### Linting and Formating

- [eslint](https://eslint.org/)
- [prettier](https://prettier.io/)
- [eslint-prettier](https://github.com/prettier/eslint-config-prettier)

### Style and CSS

- [Tailwindcss editor setup](https://tailwindcss.com/docs/editor-setup)
- a postcss plugin is required if you use `<style lang="postcss"></style>` in your vue components
- [Check out Tailwind's Documentation for help](https://tailwindcss.com/)
